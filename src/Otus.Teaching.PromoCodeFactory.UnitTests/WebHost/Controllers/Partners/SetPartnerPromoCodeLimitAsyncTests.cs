﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly SetPartnerPromoCodeLimitRequest _setPartnerPromoCodeLimitRequest;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _setPartnerPromoCodeLimitRequest = fixture.Create<SetPartnerPromoCodeLimitRequest>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNull_ReturnNotFound()
        {
            // Arrange
            var request = _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(new Guid(), _setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            //Arrange
            var partner = new Fixture().Build<Partner>()
                .With(p => p.IsActive, false)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, _setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_OnSetLimit_IfPartnerLimitNotEndAndNotCanceled_NumberIssuedPromoCodesIsZero()
        {
            //Arrange
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.EndDate, DateTime.Now.AddDays(1))
                .Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();

            var partner = new Fixture().Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 111)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnerLimit })
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, _setPartnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Theory]
        [InlineData("", "2021-1-1")]
        [InlineData("2021-1-1", "")]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerLimitEndOrCanceled_NumberIssuedPromoCodesNotZero(string limitCancelDate, string limitEndDate)
        {
            //Arrange

            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, string.IsNullOrEmpty(limitCancelDate) ? DateTime.MinValue : DateTime.Parse(limitCancelDate))
                .With(p => p.EndDate, string.IsNullOrEmpty(limitEndDate) ? DateTime.MinValue : DateTime.Parse(limitEndDate))
                .Without(p => p.Partner)
                .Create();

            var partner = new Fixture().Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 111)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnerLimit })
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, _setPartnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(111);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnBadRequest()
        {
            //Arrange
            var request = _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);

            _setPartnerPromoCodeLimitRequest.Limit = 0;

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, _setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_AddLimitToDB()
        {
            //Arrange

            var partner = new Fixture().Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, _setPartnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.Count.Should().Be(1);
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
    }

}
